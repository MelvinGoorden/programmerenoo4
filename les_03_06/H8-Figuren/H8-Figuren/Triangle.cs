﻿using System;
using System.Collections.Generic;
using System.Text;

namespace H8_Figuren
{
    class Triangle
    {
        private double height = 1.0;
        private double width = 1.0;
        private double surface;

        public double Height
        {
            get
            {
                return height;
            }
            set
            {
                if (value > 0)
                {
                    height = value;
                }
                else
                {
                    Console.WriteLine($"Het is verboden een hoogte van {value} in te stellen!");
                }
            }
        }
        public double Base
        {
            get
            {
                return width;
            }
            set
            {
                if (value > 0)
                {
                    width = value;
                }
                else
                {
                    Console.WriteLine($"Het is verboden een basis van {value} in te stellen!");
                }
            }
        }
        public double Surface
        {
            get
            {
                surface = width * height / 2;
                return surface;
            }
        }
    }
}
