﻿using System;

namespace H8_Figuren
{
    class Program
    {
        static void Main(string[] args)
        {
            Triangle triangle1 = new Triangle();
            triangle1.Base = 3;
            triangle1.Height = 1;
            Console.WriteLine($"Een driehoek met een basis van {triangle1.Base}m en een hoogte van {triangle1.Height}m heeft een oppervlakte van {triangle1.Surface}m².");

            Rectangle rectangle1 = new Rectangle();
            rectangle1.Width = 2.2;
            rectangle1.Height = 1.5;
            Console.WriteLine($"Een driehoek met een basis van {rectangle1.Width}m en een hoogte van {rectangle1.Height}m heeft een oppervlakte van {Math.Round(rectangle1.Surface, 2)}m².");
        }
    }
}
