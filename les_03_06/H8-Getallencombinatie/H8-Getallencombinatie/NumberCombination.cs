﻿using System;
using System.Collections.Generic;
using System.Text;

namespace H8_Getallencombinatie
{
    class NumberCombination
    {
        private int number1;
        private int number2;
        public int Number1
        {
            get
            {
                return number1;
            }
            set
            {
                number1 = value;
            }
        }
        public int Number2
        {
            get
            {
                return number2;
            }
            set
            {
                number2 = value;
            }
        }

        public double Sum()
        {
            double sum;
            sum = number1 + number2; 
            return sum;
        }
        public double Difference()
        {
            double difference;
            difference = number1 - number2;
            return difference;
        }
        public double Product()
        {
            double product;
            product = number1 * number2;
            return product;
        }
        public double Quotient()
        {
            double quotient = 0;
            if (number2 != 0)
            {
                quotient = Convert.ToDouble(number1) / number2;
            }
            else
            {
                Console.WriteLine("error");
            }
            return quotient;
        }

    }
}
