﻿using System;
using System.Collections.Generic;
using System.Text;

namespace H8_RapportModule_V1
{
    class ResultV1
    {
        private int percentage;
        public int Percentage
        {
            get
            {
                return percentage;
            }
            set
            {
                percentage = value;
            }
        }
        public string PrintHonors()
        {
            string honors = "not in range!";

            if (percentage < 50)
            {
                honors = "onvoldoende";
            }
            else if (percentage >= 50 && percentage < 68)
            {
                honors = "voldoende";
            }
            else if (percentage >= 68 && percentage < 75)
            {
                honors = "onderscheiding";
            }
            else if (percentage >= 75 && percentage < 85)
            {
                honors = "grote onderscheiding";
            }
            else if (percentage >= 85)
            {
                honors = "grootste onderscheiding";
            }
            else
            {
                throw new NotImplementedException();
            }


            return honors;
        }

    }
}
