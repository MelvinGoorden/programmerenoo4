﻿using System;

namespace H8_RapportModule_V1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("wat is je percentage: ");
            int percentage = Convert.ToInt32(Console.ReadLine());

            ResultV1 result1 = new ResultV1();
            result1.Percentage = percentage;
            string honors = result1.PrintHonors();

            Console.WriteLine($"je hebt een {honors}");
        }
    }
}
