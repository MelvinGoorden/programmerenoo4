﻿using System;
using System.Collections.Generic;
using System.Text;

namespace H8_dag_van_de_week
{
    class DayOfWeekProgram
    {
        public static void DayOfWeek()
        {
            Console.Write("Welke dag?");
            int day = int.Parse(Console.ReadLine());
            Console.Write("Welke maand?");
            int month = int.Parse(Console.ReadLine());
            Console.Write("Welk jaar?");
            int year = int.Parse(Console.ReadLine());
            DateTime dateTime = new DateTime(year, month, day);
            Console.WriteLine($"{dateTime.ToString("dd MMMM yyyy")} is een {dateTime.ToString("dddd")}");
        }
    }
}
