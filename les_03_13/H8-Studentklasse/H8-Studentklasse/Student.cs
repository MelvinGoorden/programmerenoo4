﻿using System;
using System.Collections.Generic;
using System.Text;

namespace H8_Studentklasse
{
    enum ClassGroups { EA1, EA2, EB1 };
    class Student
    {
        public string Name { get; set; }
        private byte age;

        public byte Age
        {
            get { return age; }
            set
            {
                if (value <= 120)
                {
                    age = value;
                }
                else
                {
                    age = 0;
                }
            }
        }

        public ClassGroups ClassGroup { get; set; }

        private byte markCommunication;

        public byte MarkCommunication
        {
            get { return markCommunication; }
            set
            {
                if (value <= 20)
                {
                    markCommunication = value;
                }
            }
        }
    }
}
