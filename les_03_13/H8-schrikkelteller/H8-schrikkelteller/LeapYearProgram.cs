﻿using System;
using System.Collections.Generic;
using System.Text;

namespace H8_schrikkelteller
{
    class LeapYearProgram
    {
        public static void Leapyears(int year1, int year2)
        {
            int counter = 0;
            for (int i = year1; i <= year2; i++)
            {
                if (DateTime.IsLeapYear(i))
                {
                    counter++;
                }

            }
            Console.WriteLine($"Er zijn {counter} schrikkeljaren tussen {year1} en {year2}.");
        }
    }
}
